## install v4l2loopback
## from https://github.com/umlaeute/v4l2loopback
unzip v4l2loopback-main.zip
cd v4l2loopback-main/
make && sudo make install
sudo depmod -a

## add a conf for video3
## from https://pypi.org/project/webcam-filters/
sudo tee /etc/modprobe.d/v4l2loopback.conf << "EOF"
# /dev/video3
options v4l2loopback video_nr=3
options v4l2loopback card_label="Virtual Webcam"
options v4l2loopback exclusive_caps=1
EOF

# install v41-utils
sudo apt install v4l-utils

## install nix, wheeee curl to sh
curl -L https://nixos.org/nix/install | sh

## activate the nix env
. /home/matt/.nix-profile/etc/profile.d/nix.sh

## install webcam-filters and deps with nix-env
nix-env     --install     --file https://github.com/jashandeep-sohi/webcam-filters/archive/refs/tags/v0.3.0.tar.gz
