## conf for video3
## from https://pypi.org/project/webcam-filters/
# sudo tee /etc/modprobe.d/v4l2loopback.conf << "EOF"
## /dev/video3
#options v4l2loopback video_nr=3
#options v4l2loopback card_label="Virtual Webcam"
#options v4l2loopback exclusive_caps=1
#EOF

## add the loopback (uses above conf)
sudo modprobe v4l2loopback

## verify with
v4l2-ctl --device /dev/video3 --info

## activate nix-env
. /home/matt/.nix-profile/etc/profile.d/nix.sh

## run webcam-filters
webcam-filters --input-dev /dev/video0 --output-dev /dev/video3 --background-blur 150
